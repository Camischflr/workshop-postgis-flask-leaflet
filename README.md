Ceci est le README du projet pour les développeurs, pour ceux qui on déjà récupéré la machine virtuelle, le tutoriel et [ici](https://gitlab.com/Oslandia/documentation/workshop-postgis-flask-leaflet/-/blob/master/roles/workshop/files/workshop/README.md)


# Installation du workshop en mode dév

Installer ansible dans un environnement virtuel

```sh
virtualenv -p python3 venv
. venv/bin/activate
python3 -m pip install ansible
```

Ensuite récupérer la machine virtuelle et la configurer
```
vagrant up
vagrant provision
```



