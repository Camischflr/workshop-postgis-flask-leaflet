import 'leaflet/dist/leaflet.css';
import L from 'leaflet/dist/leaflet.js';

var map = L.map('map').setView([45.76, 4.83], 13);

// adds OSM base map
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        })
    .addTo(map)
;

// dowload our data and adds them to map
fetch('geojson', { credentials: 'include' })
    .then(response => response.json())
    .then(data => {
        L.geoJson(data, {style:{color: "#ff7800", weight: 5, opacity: 0.65}}).addTo(map);
    })
;
